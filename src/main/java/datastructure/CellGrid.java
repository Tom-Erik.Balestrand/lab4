package datastructure;

import cellular.CellState;

public class CellGrid implements IGrid {
	
	private CellState [][] celler;

    public CellGrid(int rows, int columns, CellState initialState) {
		// TODO Auto-generated constructor stub
    	celler = new CellState[rows][columns];
    	
	}

    @Override
    public int numRows() {
        // TODO Auto-generated method stub
        return celler.length;
    }

    @Override
    public int numColumns() {
        // TODO Auto-generated method stub
        return celler[0].length;
    }

    @Override
    public void set(int row, int column, CellState element) {
        // TODO Auto-generated method stub
    	celler[row][column] = element;
        
    }

    @Override
    public CellState get(int row, int column) {
        // TODO Auto-generated method stub
        return celler[row][column];
    }

    @Override
    public IGrid copy() {
        // TODO Auto-generated method stub
    	IGrid copy = new CellGrid(numRows(),numColumns(), CellState.DEAD);
    	
    	for(int i = 0; i < numRows();i++) {
    		for (int j = 0; j < numColumns();j++) {
    			copy.set(i, j, celler[i][j]);
    		} 
    	}
    	
    	
    	return (IGrid) copy;
        
    }
    
}
